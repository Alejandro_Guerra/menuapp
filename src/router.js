import Vue from "vue";
import Doc from "@/views/Doc";
import firebase from "firebase";
import Router from "vue-router";
import LandingPage from "@/views/LandingPage";
import Cart from "@/views/Cart";

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: "/landing",
      name: "LandingPage",
      component: LandingPage
    },
    {
      path: "/",
      name: "LandingPage",
      component: LandingPage
    },
    {
      path: "/cart",
      name: "Cart",
      component: Cart,
      props: true
    },
    {
      path: "/doc",
      name: "Doc",
      component: Doc
    }
  ]
});

router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (!requiresAuth && currentUser) next("landingpage");
  else next();
});

export default router;
