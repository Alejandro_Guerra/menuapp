import axios from "axios";

export default {
  /*
   * GET all Menu Items
   */
  getMenu(success) {
    axios({
      method: "get",
      url: "https://mum-s-ba283.firebaseio.com/all.jon",
      timeout: 5000
    })
      .then(response => {
        success({ result: "ok", data: response.data });
      })
      .catch(err => {
        alert(err);
        return { result: "error", error: err };
      });
  }
};
