import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import VueMaterial from "vue-material";
import "vue-material/dist/vue-material.min.css";
import Toasted from "vue-toasted";

Vue.use(Toasted);
Vue.use(VueMaterial);
Vue.config.productionTip = false;

// Register a global custom directive called `v-focus`
Vue.directive("auto-focus", {
  // When the bound element is inserted into the DOM...
  inserted: function(el) {
    // Focus the element
    el.focus();
  }
});

const firebase = require("firebase/app");

let app = "";
/* Fire APP Config
 * For the simplicity of the app, I put the Firebase configuration in main.js file,
 * but on real production system, the config should be in a specific configuration file
 */
const configFireBase = {
  apiKey: "AIzaSyDX5eVveZuUI37xlr3wgdvAdmOQTyf7Ea4",
  authDomain: "mum-s-ba283.firebaseapp.com",
  databaseURL: "https://mum-s-ba283.firebaseio.com",
  projectId: "mum-s-ba283",
  storageBucket: "mum-s-ba283.appspot.com",
  messagingSenderId: "614958357906"
};

firebase.initializeApp(configFireBase);

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    /* eslint-disable no-new */
    app = new Vue({
      router,
      render: h => h(App)
    }).$mount("#app");
  }
});
